import sys

import scvelo as scv
import scanpy as sc
import collections.abc
from collections.abc import Iterable
import cellrank as cr
from anndata import AnnData
import numpy as np
import pandas as pd

scv.settings.verbosity = 3
scv.settings.set_figure_params("scvelo")
cr.settings.verbosity = 2

import warnings

warnings.simplefilter("ignore", category=UserWarning)
warnings.simplefilter("ignore", category=FutureWarning)
warnings.simplefilter("ignore", category=DeprecationWarning)

filename = "merge_tumor_v4_harmony_SCTransform_percent_rb_nCount_RNA_pca_40_0.45_SLIM.h5seurat"
adata = scv.read(filename, cache=True)

print("------------------------------------------------------------------------------------------")
print("................................ PRINT type of data ..........................................")
print("..........................................................................................")

print(type(adata))



print("------------------------------------------------------------------------------------------")
print("................................ PRINT adata ..........................................")
print("..........................................................................................")

print(adata)


print("------------------------------------------------------------------------------------------")
print("................................ PRINT proportions ..........................................")
print("..........................................................................................")


scv.pl.proportions(adata)

print("------------------------------------------------------------------------------------------")
print("................................  ..Filtering and Normalization........................................")
print("..........................................................................................")

scv.pp.filter_and_normalize(adata, min_shared_counts=20, n_top_genes=2000)

print("------------------------------------------------------------------------------------------")
print("................................ PRINT adata ..........................................")
print("..........................................................................................")

print(adata)

print("------------------------------------------------------------------------------------------")
print("................................ PRINT adata ..........................................")
print("..........................................................................................")

sc.tl.pca(adata)
