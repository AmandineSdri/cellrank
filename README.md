# **Trajectory**
---

## **DESCRIPTION**

This repository contains the scripts for single cell analysis under development  .... TBF ....

---

## ** PACKAGES **

CellRank and PANGA are both tools for ....
The differences are mainly based on .... (ref articles)

### PAGA 

*  [Link to install Cerebro](https://gitlab.com/bioinfo_gustaveroussy/bigr) 


The approach to make corresponding the 15 clusters obtained from seurat after single cell integration to those obtained from PAGA :
- Visualization with ccerebroApp Shiny application (image below):

<p align="center">
  <img src="https://gitlab.com/AmandineSdri/cellrank/-/blob/main/images/cerebro_clusters.png">
</p>

- 

### Cellrank

---

## **INSTALLATION**

### **ENVIRONMENT**
-  First you need to install mamba

  ``` bash
  curl -Ls https://micro.mamba.pm/api/micromamba/osx-64/latest | tar -xvj bin/micromamba
  ```
-  Then create   **Cellrank** environment thanks to .yml file
 
  ``` bash
  mamba env create -f cellrank.yml
  ```

- Activate **Cellrank** environment:

  ``` bash
  mamba activate cellrank
  ```

- Execute **Cellrank.py** script:

  ``` bash
  mamba activate cellrank
  ```


## **INPUT**

- Raw data :
  - For Cellrank 
    - .....
    - .....

  - For PAGA :
    - .....
    - .....

---


